package com.oauth2withinterface.oauth2withinterfacce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2withinterfacceApplication {

	public static void main(String[] args) {

		SpringApplication.run(Oauth2withinterfacceApplication.class, args);
	}

}
